# Create a 'class' called NeatHuman
# A class can be used to track variables that every NeatHuman has about them, like are they alive.
# A class can also be used to express functions on that human, like are they going to complain about being hungry.
class NeatHuman:
    alive = True

    # The 'init' or 'initialize' sets up the object with it's basics
    def __init__(self, name, metabolism):
        # This line takes in the incoming string called 'name' and assigns it to this object's self.name field
        self.name = name
        # Same here, but metabolism is an integer (which we don't care about yet)
        self.metabolism = metabolism

        # Everyone starts with the same hunger, let's "hard-code" it here to '10'!
        # "Hard code" is programmer slang for injecting values directly into the program (instead of injecting them in from higher up)
        self.hunger = 10

    # When we want an hour to pass on our humans (And they get hungrier), we call this function
    def one_hour_passes(self):
        new_hunger = self.hunger - self.metabolism
        self.hunger = new_hunger
        name = self.name

        # While getting hungrier - check to see if they died from hunger
        if self.hunger < 0:
            self.alive = False
            print(f"{name} just died from hunger!")

    def complain_about_it(self, name):
        # If less than 4 hunger, complain
        # Also, they can't complain if they're dead
        if self.hunger < 4 :
            print(f"I, {name}, am hungry - can we eat now?")
        else:
            print(f"I, {name}, have nothing to complain about!")
