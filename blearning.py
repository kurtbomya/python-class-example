# Import the class called "NeatHuman" from that other file 'human.py' - We'll unpack this in a minute
from human import NeatHuman

# Create a NeatHuman 'object', and name it Kurt
# If you look at the other human.py file, you see that the NeatHuman class is expecting 2 things:
#   1) a STRING meant to be the NeatHuman's name.
#   2) an INT (integer) meant to be the NeatHuman's metabolism
# Make Kurt, he has a lazy desk job and therefore metabolism of 1
person1 = NeatHuman('Kurt', 1)

# Create a second NeatHuman 'object', and name it Amber
# Amber plays frisbee on weekends and has double Kurt's metabolism (2)
person2 = NeatHuman('Amber', 2)

# Create a third NeatHuman 'object', and name it Elizabeth
# Elizabeth also plays frisbee on weekends like Amber and matches her metabolism of 2
person3 = NeatHuman('Elizabeth', 2)

# Bundle all these people into a LIST of objects called 'friends'
friends = [person1, person2, person3]

# Create a "FOR" loop that goes through the list for each item in the provided list
# There's 3 people in this list called "friends", so this function will be ran each time, against each friend
print("8AM")
for friend in friends:
    friend.one_hour_passes()
    friend.complain_about_it(friend.name)

# Now the function has been ran once, we'll see the output of 'complain about it' printed for each person in the set
# Let's print a new time, one hour later of 9AM - and run the same exact block of code.

print("9AM")
for friend in friends:
    friend.one_hour_passes()
    friend.complain_about_it(friend.name)

print("10AM")
for friend in friends:
    friend.one_hour_passes()
    friend.complain_about_it(friend.name)

print("11AM")
for friend in friends:
    friend.one_hour_passes()
    friend.complain_about_it(friend.name)

# They're certainly getting hungry by now. Let's keep pushing them!

print("12PM")
for friend in friends:
    friend.one_hour_passes()
    friend.complain_about_it(friend.name)

print("1PM")
for friend in friends:
    friend.one_hour_passes()
    friend.complain_about_it(friend.name)

# Whoops - some of them died (at least are being REALLY dramatic right now)
