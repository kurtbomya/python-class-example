# Python Class Example

### Summary

This repo was made as an example so my friends Amber & Elizabeth can grasp classes in Python.

### Scenario
- We have 3 friends; Amber, Kurt, and Elizabeth
- Amber & Elizabeth play Frisbee and have higher metabolisms than Kurt
- Kurt has a desk job, loser.
- The program builds these 3 persons and puts them in a list named `friends`.
- Each friend has one hour of time applied to them, until they die of hunger :)
- See if you can guess ahead of time who's going to die first.

### How to use
- Clone this repository to your computer
- Change to this newly imported directory
- Type `python blearning.py`. Hit enter!

### What's going on here?
- `blearning.py` imports the `NeatHuman` class from `human.py`
- The included `README.md` file is what you're reading right now

### Environment
- Written on Python 3.6.1
